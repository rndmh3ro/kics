# KICS analyzer changelog

## v1.0.1
- Bump `analyzers/report` to latest v3.7.1 to suppress misleading SARIF warning (!16)

## v1.0.0
- Promote kics to v1 (!15)
- Update report version to v3.7.0 (!15)
  - Map sarif rule name to identifier.name

## v0.1.0
- Propagate loglevel to kics (!8)

## v0.0.4
- Move sarif package to the report dependency (!5)

## v0.0.3
- Lowercase scanner id to be kics (!6)

## v0.0.2
- Switch to KICS (!1)

## v0.0.1
- init terrscan analyzer
