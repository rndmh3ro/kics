package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"

	"github.com/urfave/cli/v2"
)

const (
	outputPath = "/tmp"
	outputName = "kics"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, projectPath string) (io.ReadCloser, error) {
	reportPath := fmt.Sprintf("%s/%s.sarif", outputPath, outputName)

	log.Infof("path %s", projectPath)
	cmd := exec.Command("kics", buildArgs(projectPath)...)

	output, err := cmd.CombinedOutput()
	if err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
				exitStatus := status.ExitStatus()
				log.Debugf("Exit Status: %d", exitStatus)
				// https://docs.kics.io/latest/results/#exit_status_code
				if exitStatus > 50 {
					log.Fatalf("Encountered a system problem: %v", err)
				}
			} else {
				log.Fatalf("Couldn't get status; exiterr: %v, err: %v", exiterr, err)
			}
		} else {
			log.Fatalf("Unknown error: %v", err)
		}
	}

	log.Debugf("%s\n%s", cmd.String(), output)

	return os.Open(reportPath)
}

func buildArgs(projectPath string) []string {
	return []string{
		"scan", "--ci",
		"-p", projectPath,
		"--log-level", kicsLoglevel(),
		"--output-path", outputPath,
		"--output-name", outputName,
		"--report-formats", "sarif",
	}
}

// kicsLogLevel returns the uppercase log level equivalent
// as extracted from logrus. Conveniently, this (currently)
// matches all known log levels
func kicsLoglevel() string {
	return strings.ToUpper(log.GetLevel().String())
}
