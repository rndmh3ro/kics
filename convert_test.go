package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

func TestConvert(t *testing.T) {
	tests := []struct {
		description string
		projectPath string
		reportPath  string
		wantReport  string
		in          string
	}{
		{
			description: "kics report from a terraform repo",
			reportPath:  "testdata/kics_reports/terraform.sarif",
			wantReport:  "testdata/expect/terraform/gl-sast-report.json",
		},
	}

	for idx := range tests {
		test := tests[idx]
		t.Log(test.description)
		f, err := os.Open(test.reportPath)
		defer f.Close()
		if err != nil {
			t.Fatal(err)
		}
		gotReport, err := convert(f, test.projectPath)
		if err != nil {
			t.Fatal(err)
		}

		var wantReport *report.Report
		bytes, err := ioutil.ReadFile(test.wantReport)
		if err != nil {
			t.Fatal(err)
		}
		if err := json.Unmarshal(bytes, &wantReport); err != nil {
			t.Fatal(err)
		}

		wantReport.DependencyFiles = []report.DependencyFile{}
		wantReport.Analyzer = "kics"
		wantReport.Config.Path = ".gitlab/sast-ruleset.toml"

		if diff := cmp.Diff(wantReport, gotReport); diff != "" {
			t.Errorf("Report mismatch (-want +got):\n%s", diff)
		}
	}
}
