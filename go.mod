module gitlab.com/gitlab-org/security-products/analyzers/kics

require (
	github.com/google/go-cmp v0.5.6
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.5.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.24.0
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.7.1
)

go 1.15
